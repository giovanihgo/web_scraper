import time
from datetime import datetime

from openpyxl import Workbook
from openpyxl.styles import Style, PatternFill, Border, Side, Alignment, Protection, Font

class ExcelReport(object):


    row = 1
    col = 1

    border_bottom = Border(bottom=Side(border_style='thin', color='FF000000'))
    fill_lightblue = PatternFill(start_color='87CEFA', end_color='87CEFA', fill_type='solid')
    fill_white = PatternFill(start_color='FFFFFFFF', end_color='FFFFFFFF', fill_type='solid')

    def __init__(self, data):
        # create book
        self.book = Workbook()
        self.sheets()
        self.headers()
        self.set_products(data)

    def save(self):
        self.book.save('products.xlsx')

    def sheets(self):
        # create sheet
        self.sheet1 = self.book.active
        self.sheet1.title = 'Productos'

    def headers(self):
        self.header_products()

    def header_products(self):
        col = 1
        # insert data of product
        self.sheet1.freeze_panes = 'A2'
        ca = self.sheet1.column_dimensions['A']
        ca.width = 35
        ca = self.sheet1.column_dimensions['B']
        ca.width = 25
        ce = self.sheet1.column_dimensions['E']
        ce.width = 160
        # insert data in sheet matrix ['row', 'col']
        self.sheet1.cell(row=self.row, column=1, value='PRODUCTO')
        self.sheet1.cell(row=self.row, column=2, value='CODIGO DEL PRODUCTO')
        self.sheet1.cell(row=self.row, column=3, value='PRECIO')
        self.sheet1.cell(row=self.row, column=4, value='INVENTARIO')
        self.sheet1.cell(row=self.row, column=5, value='PRIMER ARRIBO')
        self.sheet1.cell(row=self.row, column=6, value='PIEZAS EN TRANSITO')
        self.sheet1.cell(row=self.row, column=7, value='URL DEL PRODUCTO')
        self.row += 1

    def set_products(self, products):
        for index, product in enumerate(products):
            col = 1
            # insert data of product
            # insert data in sheet matrix ['row', 'col']
            fill = self.fill_lightblue
            if index % 2 == 0:
                fill = self.fill_white

            self.sheet1.cell(row=self.row, column=1, value=product['producto_name']).fill = fill
            self.sheet1.cell(row=self.row, column=2, value=product['producto_key']).fill = fill
            self.sheet1.cell(row=self.row, column=3, value=product['precio']).fill = fill
            self.sheet1.cell(row=self.row, column=4, value=product['subtotal']).fill = fill
            self.sheet1.cell(row=self.row, column=4, value=product['arribo']).fill = fill
            self.sheet1.cell(row=self.row, column=4, value=product['transito']).fill = fill
            self.sheet1.cell(row=self.row, column=5, value=product['url']).fill = fill
            self.row += 1
