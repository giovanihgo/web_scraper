# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import csv


class CsvReport(object):
    """
    Class to create csv.
    """
    def __init__(self):
        self.stream = ''
        self.writer = ''

    def headers_products_magmi(self):
        """
        create headers to csv.
        """
        headers = [(
            'empresa',
            'sku',
            'color',
            'name',
            'description',
            'price',
            'qty',
            'url',
            'categories',
            'media_gallery',
            'image',
            'small_image',
            'thumbnail',
            'type',
            'configurable_attributes',
            'simples_skus'
        )]

        return headers

    def headers_top5_products_magmi(self):
        """
        create headers to csv.
        """
        headers = [(
            'empresa',
            'sku',
            'fecha1',
            'stock1',
            'precio1',
            'fecha2',
            'stock2',
            'precio2',
            'fecha3',
            'stock3',
            'precio3',
            'fecha4',
            'stock4',
            'precio4',
            'fecha5',
            'stock5',
            'precio5',
        )]

        return headers

    def write_rows(self, rows):
        """
        Set data of scarper to csv.
        """
        self.writer.writerows(rows)

    def create_products_magmi(self, products):
        """
        Create csv to magmi.
        """
        self.stream = open('products_scraper.csv', 'wb')
        self.writer = csv.writer(self.stream)
        data_rows = []
        headers = self.headers_products_magmi()

        data_rows += headers
        data_rows += products

        self.write_rows(data_rows)

    def create_top5_products_magmi(self, products):
        """
        Create csv to magmi.
        """
        self.stream = open('productsqty_scraper.csv', 'wb')
        self.writer = csv.writer(self.stream)
        data_rows = []
        headers = self.headers_top5_products_magmi()

        data_rows += headers
        data_rows += products

        self.write_rows(data_rows)
