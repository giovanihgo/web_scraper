# -*- coding: utf-8 -*-
import time, re

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait, Select # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

import pyodbc

from db_conection import MsSqlServerDb

def parse_price(product_price):
    product_price = product_price.replace('$', '')
    product_price = product_price.replace('MX', '')
    product_price = product_price.replace(' ', '')
    product_price = product_price.replace('*', '')
    product_price = product_price.replace(',', '')
    product_price = product_price.replace('#N/A', '')
    product_price = product_price if product_price != '' else '0'

    return product_price

def try_time_element(web_driver, type_find, value_find, xtime=10):
    try:
        element = WebDriverWait(web_driver, xtime).until(
        EC.presence_of_element_located((type_find, value_find))
        )
        return element
    except TimeoutException:
        return ''

def try_time_all_elements(web_driver, type_find, value_find, xtime=10):
    try:
        elements = WebDriverWait(web_driver, xtime).until(
        EC.presence_of_all_elements_located((type_find, value_find))
        )
        return elements
    except TimeoutException:
        return ''

def save_to_csv(company, products):
    """
    write the list of products of the website to a file csv.
    """
    myfile = open(company+'.csv', 'wb')
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(products)

def save_database(products):
    """
    set data products to database.
    """
    if len(products) > 0:
        try:
            mss_cnn = MsSqlServerDb()
            mss_cnn.open()
            mss_cnn.set_products(products)
        except pyodbc.Error, err:
            print 'ERROR PYODBC --> ', err
            save_database(products)
        finally:
            mss_cnn.close()


class ScraperSunline(object):
    """
    """
    def __init__(self):
        self.company = 'SUNLINE'
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.driver.get("http://www.sunline.com.mx/sunline2014/index.pl")
        self.search()

    def get_elements(self, products_content):
        """
        """
        products_url = []
        products = []

        # Loop the elements and get the url of categories
        for product_content in products_content:
            url = product_content.get_attribute('href')
            products_url.append(url)

        # Loop the url of categories
        for product_url in products_url:
            # go to the product and get the detail element by url
            self.driver.get(product_url)

            product_name = try_time_element(self.driver, By.CSS_SELECTOR, 'table tbody tr td span.titulo')
            product_desc = try_time_element(self.driver, By.CSS_SELECTOR, 'table tbody tr td div span.descripcion')
            product_size = try_time_element(self.driver, By.XPATH, '/html/body/table[3]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/span[2]')
            product_material = try_time_element(self.driver, By.XPATH, '/html/body/table[3]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/span[3]')
            product_tecnics = try_time_element(self.driver, By.XPATH, '/html/body/table[3]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/span[4]')

            product_stockprice = try_time_all_elements(self.driver, By.XPATH,  '/html/body/table[3]/tbody/tr[3]/td/table/tbody/tr[2]/td/span')
            product_img = try_time_element(self.driver, By.XPATH, '/html/body/table[3]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/img')
            product_img = product_img.get_attribute('src')

            codes = []
            stocks = []
            prices = []
            pza_caja = []
            peso_caja = []
            medidas = []
            transito = []
            fecha_arribo = []
            apartado = []

            for indx, table in enumerate(product_stockprice):
                td_str = table.text
                # quit sign of money
                td_str = td_str.replace('$ ','')
                # get data of text slice by '\n'
                rows_td = td_str.split('\n')

                if indx == 0:
                    codes = rows_td
                if indx == 1:
                    stocks = rows_td
                if indx == 2:
                    prices = rows_td
                if indx == 3:
                    pza_caja = rows_td
                if indx == 4:
                    peso_caja = rows_td
                if indx == 5:
                    medidas = rows_td
                if indx == 6:
                    transito = rows_td
                if indx == 7:
                    fecha_arribo = rows_td
                if indx == 8:
                    apartado = rows_td

            product_stocks = []
            # set the stock by product
            for indx, code in enumerate(codes):

                price = prices[indx] if len(codes) == len(prices) else '0'
                price = parse_price(price)
                exist = stocks[indx] if len(codes) == len(stocks) else '0'
                exist = exist.replace(',', '')
                exist = '0' if exist == '' else exist
                try:
                    exist = int(exist)
                except ValueError:
                    exist = 0
                prods = (
                    self.company,
                    str(time.strftime("%Y-%m-%d")),
                    product_url,
                    codes[indx] if len(codes) == len(codes) else '',
                    product_name.text if product_name != '' else '',
                    product_desc.text if product_desc != '' else '',
                    self.categorie,
                    '',
                    price,
                    '',
                    product_img,
                    product_size.text if product_size != '' else '',
                    product_tecnics.text if product_tecnics != '' else '',
                    '',
                    exist,
                    product_material.text if product_material != '' else ''
                )
                # set data products in list
                products.append(prods)

        return products

    def get_categories(self):
        # find list of categories
        categories = self.driver.find_elements_by_css_selector('a.menu_principal3')

        # get the url of categories
        for categorie in categories:
            try:
                categorie.find_element_by_css_selector('img')
            except:
                categorie_url = categorie.get_attribute('href')
                self.categorie_urls.append({'url': categorie_url, 'name': categorie.text})

    def search(self):
        self.products = []
        self.categorie_urls = []
        products_content = []

        self.get_categories()
        # enter the categorie and get the elements
        for categorie_url in self.categorie_urls:
            self.driver.get(categorie_url['url'])
            self.categorie = categorie_url['name']
            products_content = try_time_all_elements(self.driver, By.XPATH , "/html/body/table[2]/tbody/tr[1]/td[2]/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr/td/a")

            # products by categorie
            products_categ = self.get_elements(products_content)
            # all products
            self.products += products_categ
            # write products info to the database
            save_database(products_categ)

        # close the browser
        self.driver.quit()


class ScraperForPromotional(object):
    """
    """
    def __init__(self):
        self.company = 'FORPROMOTIONAL'
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.driver.get("http://www.forpromotional.com.mx")
        self.loggin()
        self.search()

    def loggin(self):
        # find the element that's name attribute is psw (the loggin box)
        panel_loggin = self.driver.find_element_by_id("btl-panel-login")
        panel_loggin.click()

        inpt_user = self.driver.find_element_by_id("btl-input-username")
        # type in the search
        inpt_user.send_keys("liz.promocionales@gmail.com")

        inpt_pass = self.driver.find_element_by_id("btl-input-password")
        # type in the search
        inpt_pass.send_keys("3hQyL357")

        # click boton
        btn_login = self.driver.find_element_by_name("Submit")
        btn_login.click()

        # wait to loggin
        WebDriverWait(self.driver, 30).until(
        EC.presence_of_element_located((By.ID, "btl-panel-profile"))
        )

    def get_categories(self):
        """
        """
        # find list of categories
        categories = self.driver.find_elements_by_css_selector('div.category div.spacer h2 a')

        # get the url of categories
        for categorie in categories:
            url = categorie.get_attribute('href')
            self.categorie_urls.append({ 'url': url, 'name': categorie.text })

    def get_elements(self, products_content):
        """
        """
        products = []
        products_url = []
        # Loop the elements and get the url of categories
        for product_content in products_content:
            products_url.append(product_content.get_attribute('href'))

        # Loop the url of categories
        for product_url in products_url:
            # go to the product and get the detail element by url
            self.driver.get(product_url)
            product_code = try_time_element(self.driver, By.CSS_SELECTOR, 'html body div#top-bar div#doc-title span.name')
            product_name = try_time_element(self.driver, By.CSS_SELECTOR, "div.productdetails-view.productdetails div h1")
            product_desc = try_time_element(self.driver, By.CSS_SELECTOR, "div.productdetails-view.productdetails div div.product-short-description")
            product_img = try_time_element(self.driver, By.CSS_SELECTOR, "div.main-image img#medium-image.medium-image")
            product_img_url = product_img.get_attribute('src')
            product_price = try_time_element(self.driver, By.CSS_SELECTOR, "div.PricesalesPrice span.PricesalesPrice")
            product_materia = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-field.product-field-type-I:nth-of-type(1) span.product-field-display")
            product_sizes = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-field.product-field-type-I:nth-of-type(2) span.product-field-display")
            product_pack_quty = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-field.product-field-type-I:nth-of-type(3) span.product-field-display")
            product_size_print = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-field.product-field-type-I:nth-of-type(5) span.product-field-display")
            product_tecnics = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-field.product-field-type-I:nth-of-type(6) span.product-field-display")

            if product_tecnics == '':
                product_tecnics = product_size_print
                product_size_print = ''

            code = product_code.text if product_code != '' else ''
            name = product_name.text if product_name != '' else ''
            desc = product_desc.text if product_desc != '' else ''
            img = product_img_url
            price = product_price.text if product_price != '' else ''
            price = parse_price(price)
            material = product_materia.text if product_materia != '' else ''
            sizes = product_sizes.text if product_sizes != '' else ''
            pack_quty = product_pack_quty.text if product_pack_quty != '' else ''
            size_print = product_size_print.text if product_size_print != '' else ''
            tecnics = product_tecnics.text if product_tecnics != '' else ''
            modelo = ''
            color = ''
            existencias = ''
            apartado = ''
            arribos = ''
            fecha_arribo = ''

            googdocs = try_time_element(self.driver, By.CSS_SELECTOR, "div.product-description iframe")
            if googdocs:
                url_googdocs = googdocs.get_attribute('src')

                self.driver.get(url_googdocs)

                googdocs = try_time_element(self.driver, By.CSS_SELECTOR, "div iframe")
                url_googdocs = googdocs.get_attribute('src')

                self.driver.get(url_googdocs)

                try:
                    product_stocks = try_time_all_elements(self.driver, By.CSS_SELECTOR, "table.waffle tbody tr + tr")
                except TimeoutException:
                    product_stocks = []

                for tr in product_stocks:
                    modelo = tr.find_element_by_css_selector("td:nth-of-type(1)").text
                    color = tr.find_element_by_css_selector("td:nth-of-type(2)").text
                    existencias = try_time_element(tr, By.CSS_SELECTOR, "td:nth-of-type(3)")
                    existencias = existencias.text if existencias != '' else '0'
                    existencias = '0' if existencias == '' else existencias
                    existencias = '0' if existencias == '#N/A' else existencias
                    apartado = tr.find_element_by_css_selector("td:nth-of-type(4)").text
                    arribos = tr.find_element_by_css_selector("td:nth-of-type(5)").text
                    fecha_arribo = tr.find_element_by_css_selector("td:nth-of-type(6)").text

                    prods = (
                        self.company,
                        str(time.strftime("%Y-%m-%d")),
                        product_url,
                        code,
                        name,
                        desc,
                        self.categorie,
                        '',
                        price,
                        color,
                        img,
                        sizes,
                        tecnics,
                        '',
                        int(existencias),
                        material
                    )

                    # set data products in list
                    products.append(prods)

        return products

    def search(self):
        self.categorie_urls = []
        products_content = []
        self.products = []

        # go to the all products
        self.driver.get('http://www.forpromotional.com.mx/index.php/virtuemart')
        self.get_categories()

        # enter the categories
        for categorie_url in self.categorie_urls:
            self.driver.get(categorie_url['url'])
            self.categorie = categorie_url['name']
            try:
                # get the elements
                products = self.driver.find_element_by_css_selector("div.spacer div.center > a")
                if products:
                    # select all products in the list
                    select_quty = self.driver.find_element_by_css_selector("select.inputbox")
                    if select_quty:
                        inp_select = Select(self.driver.find_element_by_css_selector("select.inputbox"))
                        inp_select.select_by_visible_text("150")

                    # get the products
                    products_content = WebDriverWait(self.driver, 30).until(
                    EC.presence_of_all_elements_located((By.CSS_SELECTOR , "div.spacer div.center > a"))
                    )

                    # products by categorie
                    products_categ = self.get_elements(products_content)
                    # all products
                    self.products += products_categ

                    # write products info to the database
                    save_database(products_categ)

            except NoSuchElementException:
                self.get_categories()

        # close the browser
        self.driver.quit()


class ScraperCdoPromocionales(object):

    def __init__(self):
        self.company = 'CDOPROMOCIONALES'
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.driver.get("http://mexico.cdopromocionales.com/")
        self.loggin()
        self.search()

    def loggin(self):
        # find the element that's name attribute is psw (the loggin box)
        inpt_user = self.driver.find_element_by_id("user_login")
        # type in the search
        inpt_user.send_keys("102118")

        inpt_pass = self.driver.find_element_by_id("user_password")
        # type in the search
        inpt_pass.send_keys("LEHD800710A9A")

        # click boton
        btn_login = self.driver.find_element_by_name("commit")
        btn_login.click()

        # wait to loggin
        WebDriverWait(self.driver, 30).until(
        EC.presence_of_element_located((By.ID, "user-details"))
        )

    def get_elements(self, products_content):
        products = []
        products_url = []
        # Loop the elements and get the url of categories
        for product_content in products_content:
            products_url.append(product_content.get_attribute('href'))

        # Loop the url of categories
        for product_url in products_url:
            try:
                # go to the product and get the detail element by url
                self.driver.get(product_url)
                product = WebDriverWait(self.driver, 30).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, "article.product"))
                )
                product_code = product.find_element_by_css_selector("div.details div.code span.value")
                product_name = product.find_element_by_css_selector("div.details h1.name")
                product_desc = product.find_element_by_css_selector("div.details div.description")
                product_img = product.find_element_by_css_selector("div.picture div.content img").get_attribute("src")
                product_tecnics = product.find_elements_by_css_selector("div.details div.icons-and-in-transit-info div.icons ul.icons-list li.icon img")
                product_price_dist = product.find_element_by_css_selector("div.details div.prices div.price.list-price span.value")
                product_price_pers = product.find_element_by_css_selector("div.details div.prices div.price.net-price span.value")
                product_price = product_price_pers.text if product_price_pers != '' else ''
                price_before_coma = product_price[:product_price.rindex(',')]
                price_last_coma = product_price[product_price.rindex(','):]
                price_last_coma = price_last_coma.replace(',', '.')
                product_price = price_before_coma + price_last_coma
                product_price = parse_price(product_price)
                product_iva_disclaimer = product.find_element_by_css_selector("div.details div.prices div.iva-disclaimer").text
                product_stock = product.find_elements_by_css_selector("div.picture div.stock-table table.stock tbody tr.variant")

                tecnics = []
                prod_tecnics = ''
                for tecnic in product_tecnics:
                    tecnics.append(tecnic.get_attribute('title'))
                    prod_tecnics += tecnic.get_attribute('title')
                    prod_tecnics += ', '

                stocks = []
                for by_color in product_stock:
                    color = by_color.find_element_by_css_selector("td.color a").get_attribute("title")
                    exist = by_color.find_element_by_css_selector("td.stock-existent")
                    exist = exist.text if exist else '0'
                    exist = exist.replace('.', '')
                    available = by_color.find_element_by_css_selector("td.stock-available")
                    # stocks.append({'color': color, 'exist': exist, 'available': available.text})

                    prods = (
                        self.company,
                        str(time.strftime("%Y-%m-%d")),
                        product_url,
                        product_code.text if product_code != '' else '',
                        product_name.text if product_name != '' else '',
                        product_desc.text,
                        self.categorie,
                        '',
                        product_price,
                        color,
                        product_img,
                        '',
                        prod_tecnics,
                        '',
                        int(exist),
                        ''
                    )

                    # set data products in list
                    products.append(prods)

            except TimeoutException:
                pass

        return products

    def get_categories(self):
        # go to the all products
        self.driver.get('http://mexico.cdopromocionales.com/products?categoria=61')

        # find list of categories
        categories = self.driver.find_elements_by_css_selector('ul#categories-list li a')

        # get the url of categories
        for categorie in categories:
            url = categorie.get_attribute('href')
            self.categorie_urls.append({'url': url, 'name': categorie.text})

    def search(self):
        self.categorie_urls = []
        self.products = []

        # get categories
        self.get_categories()

        # enter the categorie and get the elements
        for categorie_url in self.categorie_urls:
            self.driver.get(categorie_url['url'])

            self.categorie = categorie_url['name']

            products_content = WebDriverWait(self.driver, 30).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR , "div.content div.picture_and_icons a.foto"))
            )

            # products by categorie
            products_categ = self.get_elements(products_content)
            # all products
            self.products += products_categ

            # write products info to the database
            save_database(products_categ)

        # close the browser
        self.driver.quit()


class ScraperG4Mexico(object):
    """
    """

    def __init__(self):
        self.company = 'G4DEMEXICO'
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.driver.get("http://www.g4demexico.com.mx")
        self.loggin()
        self.search()

    def loggin(self):
        """
        """
        # find inputs loggin
        link_log = self.driver.find_element_by_class_name("authorization-link")
        link_log.click()

        inpt_user = WebDriverWait(self.driver, 20).until(
        EC.presence_of_element_located((By.ID, "email"))
        )
        inpt_password = self.driver.find_element_by_id("pass")

        # set values to logg
        inpt_user.send_keys('jandi@brandandgifts.mx')
        inpt_password.send_keys('g4mexico47')
        # button loggin
        inpt_log = self.driver.find_element_by_id('send2')
        inpt_log.click()
        # wait to loggin
        WebDriverWait(self.driver, 30).until(
        EC.presence_of_element_located((By.ID, "store.menu"))
        )

    def get_elements(self, products_content):
        products = []
        products_url = []
        # Loop the elements and get the url of categories
        for product_content in products_content:
            products_url.append(product_content.get_attribute('href'))

        # Loop the url of categories
        for product_url in products_url:
            # go to the product and get the detail element by url
            self.driver.get(product_url)

            try:


                product_code = try_time_element(self.driver, By.CSS_SELECTOR, 'div.product-info-stock-sku div.product.attibute.sku div.value')
                product_name = try_time_element(self.driver, By.CSS_SELECTOR, 'div.page-title-wrapper.product h1.page-title span.base')
                product_material = try_time_element(self.driver, By.CSS_SELECTOR,  'table#product-attribute-specs-table.data.table.additional-attributes tbody tr td.col.data')
                product_tecnics = ''
                product_img = try_time_element(self.driver, By.CSS_SELECTOR, 'img.fotorama__img').get_attribute("src")
                product_desc = ''
                product_sub_categorie = ''
                product_mh = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[2]/td')
                product_mw = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[3]/td')
                product_ml = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[4]/td')

                product_mh = product_mh.text if product_mh != '' else ''
                product_mw = product_mw.text if product_mw != '' else ''
                product_ml = product_ml.text if product_ml != '' else ''

                product_ph = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[7]/td')
                product_pw = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[8]/td')
                product_pl = try_time_element(self.driver, By.XPATH, '/html/body/div[1]/main/div[2]/div/div[1]/div[4]/table/tbody/tr[9]/td')

                product_ph = product_ph.text if product_ph != '' else ''
                product_pw = product_pw.text if product_pw != '' else ''
                product_pl = product_pl.text if product_pl != '' else ''

                product_pack_size = product_mh+'x'+product_mw+'x'+product_ml
                product_size = product_ph+'x'+product_pw+'x'+product_pl

                product_price = try_time_element(self.driver, By.CSS_SELECTOR, 'span.price')
                product_price = product_price.text if product_price != '' else ''
                product_price = parse_price(product_price)

                color_stocks = try_time_all_elements(self.driver, By.CSS_SELECTOR, '.color-stock')

                for color_stock in color_stocks:
                    if color_stock != '':
                        product_colstc = color_stock.text
                        product_cs = product_colstc.split(' - ')
                        if len(product_cs) > 0:
                            product_color = product_cs[0]
                            product_stock = product_cs[1] if len(product_cs) > 1 else '0'
                            product_stock = product_stock.replace(',', '')
                            product_stock = int(product_stock)
                        else:
                            product_color = color_stock.text
                            product_stock = 0
                    else:
                        product_color = ''
                        product_stock = 0

                    prods = (
                        self.company,
                        str(time.strftime("%Y-%m-%d")),
                        product_url,
                        product_code.text if product_code != '' else '',
                        product_name.text if product_name != '' else '',
                        product_desc,
                        self.categorie,
                        product_sub_categorie,
                        product_price,
                        product_color,
                        product_img,
                        product_size,
                        product_tecnics,
                        product_pack_size,
                        product_stock,
                        product_material.text if product_material != '' else ''
                    )

                    # set data products in list
                    products.append(prods)

            except Exception as e:
                pass

        return products

    def get_categories(self):
        """
        """
        catgs = []
        # find list of categories
        categories = self.driver.find_elements_by_css_selector('li.ui-menu-item.level1 a')
        for categorie in categories:
            # get the url
            next_url = categorie.get_attribute('href')
            span_url = categorie.find_element_by_css_selector('span')
            cat_name = span_url.text
            # add the url to list
            self.categorie_urls.append({ 'url': next_url, 'name': cat_name })
            catgs.append(next_url)

        # find url of categories pages
        for categorie_url in self.categorie_urls:
            # get the page of the url
            self.driver.get(categorie_url['url'])

            try:
                pages = self.driver.find_element_by_css_selector("div.pager a")
                if '?p=' in str(categorie_url['url']):
                    url = str(categorie_url['url']).split('?p=',1)[0]
                    page = str(categorie_url['url']).split('?p=',1)[1]
                    page = int(page)
                    page += 1
                    next_url = url+'?p='+str(page)
                else:
                    next_url = str(categorie_url['url'])+'?p=2'

                if next_url not in catgs:
                    self.categorie_urls.append({ 'url': next_url, 'name': categorie_url['name'] })
                    catgs.append(next_url)

            except NoSuchElementException:
                pass

    def search(self):
        self.categorie_urls = []
        self.products = []
        products_content = []

        self.get_categories()

        for categorie_url in self.categorie_urls:

            self.driver.get(categorie_url['url'])

            self.categorie = categorie_url['name']

            products_content = self.driver.find_elements_by_css_selector("a.product-preview__more")

            # products by categorie
            products_categ = self.get_elements(products_content)
            # all products
            self.products += products_categ

            # write products info to the database
            save_database(products_categ)

        # close the browser
        self.driver.quit()

    def get_stock(self):
        return self.products


class ScraperDobleVela(object):

    def __init__(self):
        self.company = 'DOBLEVELA'
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.driver.get("http://www.doblevela.com/")
        self.loggin()
        self.search()

    def loggin(self):
        # find the element that's name attribute is psw (the email box)
        inpt_email = self.driver.find_element_by_name("email_address")
        # type in the search
        inpt_email.send_keys("raul.torres.marquez@gmail.com")

        # find the element that's name attribute is psw (the password box)
        inpt_password = self.driver.find_element_by_name("password")
        inpt_password.send_keys("doblevela"+ Keys.ENTER)

        # wait to loggin
        logged = WebDriverWait(self.driver, 30).until(
        EC.presence_of_element_located((By.ID, "loggedInMainWelcome"))
        )

    def get_elements(self, products_content):
        products_url = []
        products = []
        # Loop the elements and get the url of categories
        for product_content in products_content:
            products_url.append(product_content.get_attribute('href'))

        # Loop the url of categories
        for product_url in products_url:
            # Go to the product and get the detail element by url
            self.driver.get(product_url)
            product_code = try_time_element(self.driver, By.ID, "productsModel")
            product_name = try_time_element(self.driver, By.ID, "productsNameTrail")
            product_desc = try_time_element(self.driver, By.CLASS_NAME, "descripcionAdicional")
            product_material = try_time_element(self.driver, By.CLASS_NAME, "descripcionMaterial")
            product_sizes = try_time_element(self.driver, By.CLASS_NAME, "descripcionMedida")
            product_pack_quty = try_time_element(self.driver, By.CLASS_NAME, "descripcionEmpaque")
            product_paqsizes = try_time_element(self.driver, By.CLASS_NAME, "descripcionCajaMedida")
            product_image = try_time_element(self.driver, By.ID, 'productMainImageIMG')
            if product_image != '':
                product_image = product_image.get_attribute('src')

            product_stock = try_time_element(self.driver, By.ID, "productInfoDetailsListHeadingText")
            stock_prices = try_time_all_elements(self.driver, By.CLASS_NAME,"tableInventarioTrRow")
            product_categorie = try_time_element(self.driver, By.CSS_SELECTOR,"div#productInfoCategoryImg a img")
            product_categorie = product_categorie.get_attribute('src') if product_categorie != '' else ''
            product_categorie = product_categorie.replace('images/categories/categoria_', '')
            product_categorie = product_categorie.replace('.png', '')
            product_categorie = product_categorie.replace('http://www.doblevela.com/', '')

            clave = ''
            color = ''
            stock = 0
            public_price = ''
            dist_price = ''
            stocks = []

            total = 0
            for stock_price in stock_prices:
                clave = try_time_element(stock_price, By.CLASS_NAME, 'tdClave')
                try:
                    color = stock_price.find_element_by_class_name('tdColor')
                except NoSuchElementException:
                    color = ''

                stock = try_time_element(stock_price, By.CLASS_NAME, 'tdExistencia')
                stock = int(stock.text) if stock != '' else 0
                public_price = try_time_element(stock_price, By.CLASS_NAME, 'tdPublico')
                dist_price = try_time_element(stock_price, By.CLASS_NAME, 'tdDistribuidor')
                public_price = public_price.text if public_price != '' else '0'
                dist_price = dist_price.text if dist_price != '' else '0'
                public_price = parse_price(public_price)
                dist_price = parse_price(dist_price)

                dist_price = float(dist_price)

                total += stock

                # stocks.append({'clave': clave, 'color': color, 'stock': stock, 'public_price': public_price, 'dist_price': dist_price})

                prod = (
                    self.company,
                    str(time.strftime("%Y-%m-%d")),
                    product_url,
                    clave.text if clave != '' else '',
                    product_name.text if product_name != '' else '',
                    product_desc.text if product_desc != '' else '',
                    product_categorie,
                    '',
                    dist_price,
                    color.text if color != '' else '',
                    product_image,
                    product_sizes.text if product_sizes != '' else '',
                    '',
                    product_paqsizes.text if product_paqsizes != '' else '',
                    stock,
                    product_material.text if product_material != '' else ''
                )
                # set data products in list
                products.append(prod)

        return products

    def get_categories(self):
        next_url = "http://www.doblevela.com/index.php?main_page=advanced_search_result&keyword=%25&sort=20a&page=1"

        # loop the urls of the page
        while next_url not in self.categorie_urls:
            # get the page of the url
            self.driver.get(next_url)
            # add the url to list
            self.categorie_urls.append(self.driver.current_url)
            # find next url element
            try:
                next_page = self.driver.find_element_by_css_selector("html body#advancedsearchresultBody div#mainWrapper div#contentMainWrapperDiv table#contentMainWrapper tbody tr td div#advSearchDefault.centerGeneral div.main_contenido div#productListing div#newProductsDefaultListingBottomNumber.navSplitPagesResult.forward a:nth-of-type(2)")
                # get the url of the next page
                next_url = next_page.get_attribute('href')
            except NoSuchElementException:
                pass

        # quit the last page items repeat
        self.categorie_urls.pop(-1)

    def search(self):
        self.categorie_urls = []
        self.products = []
        products_content = []

        self.get_categories()

        # enter the categories
        for categorie_url in self.categorie_urls:
            # Enter the url
            self.driver.get(categorie_url)
            # scroll down to the bottom of a page
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            # get all results of search
            products_content = try_time_all_elements(self.driver, By.CSS_SELECTOR , "div.product_listing_details a")
            products_categ = self.get_elements(products_content)
            # all products
            self.products += products_categ

            # write products info to the database
            save_database(products_categ)

        # close the browser
        self.driver.quit()

    def get_stock(self):
        return self.products


class ScraperPromoopcion(object):

    def __init__(self):
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Firefox()
        # go to the home page
        self.site_url = 'http://www.promoopcion.com'
        self.company = 'PROMOOPCION'
        self.driver.get(self.site_url)
        self.loggin()
        self.search()

    def loggin(self):
        # find the element that's name attribute is psw (the loggin box)
        inpt_login = self.driver.find_element_by_id("psw")
        # type in the search
        inpt_login.send_keys("00138CTM")

        # click boton
        btn_login = self.driver.find_element_by_id("btnEn2")
        btn_login.click()

        # accept alert
        WebDriverWait(self.driver, 30).until(EC.alert_is_present())
        alert = self.driver.switch_to_alert()
        alert.accept()

        # quit promo
        modal_promo = try_time_element(self.driver, By.ID, 'myModal111', 10)
        button_promo = try_time_element(modal_promo, By.ID, 'bye', 10)
        if button_promo != '':
            button_promo.click()
        #ActionChains(self.driver).move_to_element(modal_promo).click().perform()


    def parse_int(self, price):
        try:
            # conver to number and quit commas
            price = int(price.replace(',', ''))
        except ValueError:
            price = 0

        return price

    def get_elements(self, products_content):
        products_url = []
        products = []
        # Loop the elements and get the url of categories
        for product_content in products_content:
            products_url.append(product_content.get_attribute('href'))

        # Loop the url of categories
        for product_url in products_url:
            # go to the product and get the detail element by url
            self.driver.get(product_url)

            product_code = try_time_element(self.driver, By.CSS_SELECTOR, 'html body div.page-header1 div.col-md-5 table.table-hover.table-responsive thead tr td.table-responsive div.col-md-5 h3', 0)
            product_name = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/thead/tr/td/h6[1]', 0)
            product_desc = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/thead/tr/td/h6[2]', 0)
            product_desc = product_desc.text if product_desc != '' else ''
            product_desc = product_desc.replace(u'Descripción: ', '')
            product_price = try_time_element(self.driver, By.CSS_SELECTOR, 'div#precios h5 strong', 5)
            product_price = product_price.text if product_price != '' else ''
            product_price = product_price.replace('$', '')
            product_price = product_price.replace(',', '')
            warehouse = try_time_all_elements(self.driver, By.CSS_SELECTOR, 'div#ex h6 strong', 10)
            stocks = try_time_all_elements(self.driver, By.CSS_SELECTOR, 'div#ex h6 font', 10)
            material = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/tbody/tr[3]/td[2]', 0)
            medidas = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/tbody/tr[4]/td[2]', 0)
            color = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/tbody/tr[6]/td[2]', 0)
            tecnicas = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/tbody/tr[9]/td[2]', 0)
            pack_medidas = try_time_element(self.driver, By.XPATH, '/html/body/div[13]/div[3]/table/tbody/tr[12]/td[2]', 0)
            img = try_time_element(self.driver, By.CSS_SELECTOR, 'img.img-circle.img-thumbnail', 0)
            img_url = img.get_attribute('src') if type(img) != 'string' else img

            try:
                feche = self.driver.find_element_by_id("fech")
                feches = feche.text.split("\n")
                arribo = feches[0].replace('Primer arribo: ','')
                transit = feches[1].replace(u'Piezas en tránsito: ', '')
            except NoSuchElementException:
                arribo = ''
                transit = ''

            house_stock = []
            total = 0

            if warehouse != '':
                # loop stocks of product and sum
                for indx, stock in enumerate(stocks):
                    stock_int = self.parse_int(stock.text)
                    total += stock_int

                    house_stock.append({'sucursal': warehouse[indx].text, 'stock': stock_int})

            prod = (
                self.company,
                str(time.strftime("%Y-%m-%d")),
                product_url,
                product_code.text if product_code != '' else '',
                product_name.text if product_name != '' else '',
                product_desc,
                self.categorie,
                '',
                float( product_price) if product_price != '' else 0,
                color.text if color != '' else '',
                img_url,
                medidas.text if medidas != '' else '',
                tecnicas.text if tecnicas != '' else '',
                pack_medidas.text if pack_medidas != '' else '',
                total,
                material.text if material != '' else ''
            )

            # set data products in list
            products.append(prod)

        return products

    def get_categories(self):
        categories = self.driver.find_elements_by_css_selector('ul.menFooter2 li.no lo a')

        # get the url of categories
        for categorie in categories:
            url = categorie.get_attribute('href')
            self.categorie_urls.append({ 'url': url, 'name': categorie.text })

    def search(self):
        self.categorie_urls = []
        self.products = []
        products_content = []

        # find list of categories
        li_categories = self.driver.find_element_by_xpath('/html/body/nav/div[2]/ul/li[3]/a')
        # show categories
        li_categories.click()
        # get categories
        self.get_categories()

        # enter the categories
        for indx, categorie_url in enumerate(self.categorie_urls):
            # Enter the url
            self.driver.get(categorie_url['url'])

            self.categorie = categorie_url['name']

            try:
                # get the elements
                products = self.driver.find_element_by_css_selector("html body div.page-header2 div.col-md-12 div.col-sm-9 div.col-md-6")
                if products:
                    # select all products in the list of dropbox
                    select_quty = Select(self.driver.find_element_by_css_selector("select#cant"))
                    if select_quty:
                        select_quty.select_by_value("100000")

                        # scroll down to the bottom of a page
                        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                        # get the products
                        products_content = try_time_all_elements(self.driver, By.CSS_SELECTOR , "html body div.page-header2 div.col-md-12 div.col-sm-9 div.col-md-6 div.thumbnail div.col-md-7 a")

                        # products by categorie
                        products_categ = self.get_elements(products_content)

                        # all products
                        self.products += products_categ

                        # write products info to the database
                        save_database(products_categ)

            except NoSuchElementException:
                self.get_categories()

        # close the browser
        self.driver.quit()

    def get_stock(self):
        return self.products
