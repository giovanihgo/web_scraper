import pyodbc
import configparser


class MsSqlServerDb(object):
    """
    Class settings database.
    """

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')

        self._host = config.get('DbConnection', 'host')
        self._port = config.get('DbConnection', 'port')
        self._user = config.get('DbConnection', 'user')
        self._passwd = config.get('DbConnection', 'pwd')
        self._db = config.get('DbConnection', 'db')
        self._driver = 'MySQL ODBC 5.3 Unicode Driver'

    def open(self):
        """
        Open contection MYSQL-Server.
        """
        self._conn_str = 'DRIVER={};SERVER={};PORT={};UID={};PWD={};DATABASE={};'.format(self._driver, self._host, self._port, self._user, self._passwd, self._db)
        self._conn = pyodbc.connect(self._conn_str)

    def close(self):
        self._conn.close()

    def set_products(self, products):
        """
        Function that insert data of products.
        """
        self._cursor = self._conn.cursor()

        query = "INSERT INTO scraper_product (empresa, fecha, url_producto, codigo, nombre, descripcion, categoria, sub_categoria, precio, color, url_imagen, medidas_producto, tecnicas, medidas_empaque, existencia, material) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

        self._cursor.executemany(query, products)
        self._conn.commit()
        self._cursor.close()

    def get_simple_products(self):
        """
        Get all simple products.
        """
        self._cursor = self._conn.cursor()

        query = """
                SELECT DISTINCT
                empresa,
                empresa + '___' + Codigo AS 'sku',
                color,
                nombre AS 'name',
                descripcion AS 'description',
                precio AS 'price',
                existencia AS 'qty',
                url_producto AS 'url',
                '' AS 'categories',
                url_imagen AS 'media_gallery',
                url_imagen AS 'image',
                url_imagen AS 'small_image',
                url_imagen AS 'thumbnail',
                'simple' AS 'type',
                '' AS 'configurable_attributes',
                '' AS 'simples_skus'
                FROM scraper_product
                WHERE
                fecha BETWEEN '2016-07-18' AND '2016-07-21'
                """

        self._cursor.execute(query)
        data = self._cursor.fetchall()
        self._cursor.close()

        return data

    def get_configurable_products(self):
        """
        Get all configurable products.
        """
        self._cursor = self._conn.cursor()

        query = """
                SELECT DISTINCT
                a.empresa,
                CONCAT(a.empresa,'___',a.clave_madre) AS 'sku',
                '' AS 'color',
                a.nombre AS 'name',
                a.descripcion AS 'description',
                a.precio AS 'price',
                '' AS 'qty',
                a.url_producto AS 'url',
                b.categories,
                a.url_imagen AS 'media_gallery',
                a.url_imagen AS 'image',
                a.url_imagen AS 'small_image',
                a.url_imagen AS 'thumbnail',
                'configurable' AS 'type',
                'color' AS 'configurable_attributes',
                c.simples_skus
                FROM scraper_product AS a
                INNER JOIN
                (
                SELECT clave_madre, GROUP_CONCAT( DISTINCT

                CONCAT(empresa,'/',categoria,'/',sub_categoria,';;Todos los productos/',categoria,'/',sub_categoria,';;')
                )  AS 'categories'
                FROM scraper_product
                GROUP BY clave_madre
                ORDER BY clave_madre

                ) AS b ON b.clave_madre = a.clave_madre
                INNER JOIN
                (
                SELECT clave_madre, GROUP_CONCAT( DISTINCT
                CONCAT(empresa, '___' , codigo) )AS 'simples_skus'
                FROM scraper_product
                GROUP BY clave_madre
                ORDER BY clave_madre

                ) AS c ON c.clave_madre = a.clave_madre
                WHERE
                a.fecha BETWEEN '2016-08-01' AND '2016-08-04'
                GROUP BY sku
                ORDER BY a.empresa, a.clave_madre
                """

        self._cursor.execute(query)
        data = self._cursor.fetchall()
        self._cursor.close()

        return data

    def set_claves_products(self):
        """
        Update claves to patterns.
        """

        query = """
                /* UPDATE FIELD COLOR FROM CODIGO IF HAVE COLOR WITH SLASH CHARACTER AND 3 SPACES IN CODIGO*/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    spt.codigo,
                    spt.color,
                    RIGHT(spt.codigo, CHAR_LENGTH( SUBSTRING_INDEX(spt.codigo, ' ', -1) ) ) AS newcolor
                    FROM
                    scraper_product AS spt
                    WHERE
                    CHAR_LENGTH(spt.color)-CHAR_LENGTH(REPLACE(spt.color, '/', '')) > '0'
                    AND
                    CHAR_LENGTH(spt.codigo)-CHAR_LENGTH(REPLACE(spt.codigo, ' ', '')) = '2'
                ) AS b ON b.codigo = a.codigo
                SET
                a.color = b.newcolor
                WHERE
                a.codigo = b.codigo
                AND
                CHAR_LENGTH(a.color)-CHAR_LENGTH(REPLACE(a.color, '/', '')) > '0'
                AND
                CHAR_LENGTH(a.codigo)-CHAR_LENGTH(REPLACE(a.codigo, ' ', '')) = '2';

                /********* UPDATE FIELD COLOR FROM CODIGO IF HAVE '-' IN CODIGO *****/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    color,
                    spt.codigo,
                    RIGHT(spt.codigo, CHAR_LENGTH( SUBSTRING_INDEX(spt.codigo, '-', -1) ) ) AS newcolor
                    FROM scraper_product AS spt
                    WHERE CHAR_LENGTH(spt.codigo)-CHAR_LENGTH(REPLACE(spt.codigo, '-', '')) = '3'
                )
                AS b ON b.codigo = a.codigo
                SET
                a.color = b.newcolor
                WHERE
                a.codigo = b.codigo
                AND
                CHAR_LENGTH(a.codigo)-CHAR_LENGTH(REPLACE(a.codigo, '-', '')) = '3';

                /*UPDATE FIELD CLAVEMADRE FROM CODIGO WITH SPACES*/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    spt.codigo,
                    SUBSTRING(spt.codigo, 1, CHAR_LENGTH(spt.codigo)-1) AS newcode
                    FROM scraper_product AS spt
                    WHERE LENGTH(spt.codigo)-LENGTH(REPLACE(spt.codigo, ' ', '')) = '2'
                ) AS b ON a.codigo = b.codigo
                SET
                a.clave_madre = b.newcode
                WHERE LENGTH(a.codigo)-LENGTH(REPLACE(a.codigo, ' ', '')) = '2';

                /*UPDATE FIELD CLAVEMADRE FROM CODIGO WITH DOT*/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    fecha,
                    spt.codigo,
                    SUBSTRING(spt.codigo, 1, (CHAR_LENGTH(spt.codigo) - CHAR_LENGTH( SUBSTRING_INDEX(spt.codigo, '.', -1) ) -1 ) ) AS newcode
                    FROM
                    scraper_product AS spt
                    WHERE CHAR_LENGTH(spt.codigo) - CHAR_LENGTH(REPLACE(spt.codigo, '.', '')) = '1'
                ) AS b ON b.codigo = a.codigo
                SET
                a.clave_madre = b.newcode
                WHERE CHAR_LENGTH(a.codigo) - CHAR_LENGTH(REPLACE(a.codigo, '.', '')) = '1';

                /* UPDATE FIELD CLAVEMADRE FROM CODIGO IF HAVE '-' IN CODIGO *****/
                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    spt.codigo,
                    spt.color,
                    SUBSTRING(spt.codigo, 1, (CHAR_LENGTH(spt.codigo) - CHAR_LENGTH( SUBSTRING_INDEX(spt.codigo, '-', -1) ) -1 ) ) AS newcode
                    FROM scraper_product AS spt
                    WHERE CHAR_LENGTH(spt.codigo) - CHAR_LENGTH(REPLACE(spt.codigo, '-', '')) = '3'
                ) AS b ON b.codigo = a.codigo
                SET
                a.clave_madre = b.newcode
                WHERE CHAR_LENGTH(a.codigo) - CHAR_LENGTH(REPLACE(a.codigo, '-', '')) = '3';

                /*******************UPDTAE CLAVEMADRE SUNLINE****************************/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    spt.codigo,
                    spt.color,
                    SUBSTRING(spt.codigo, 1, (CHAR_LENGTH(spt.codigo) - CHAR_LENGTH( SUBSTRING_INDEX(spt.codigo, '-', -1) ) -1 ) ) AS newcode
                    FROM scraper_product AS spt
                    WHERE
                    CHAR_LENGTH(spt.codigo) - CHAR_LENGTH(REPLACE(spt.codigo, '-', '')) > '0'
                    AND spt.empresa = 'SUNLINE'
                ) AS b ON b.codigo = a.codigo
                SET
                a.clave_madre = b.newcode
                WHERE
                CHAR_LENGTH(a.codigo) - CHAR_LENGTH(REPLACE(a.codigo, '-', '')) > '0'
                AND
                a.empresa = 'SUNLINE';

                /******************UPDATE CLAVEMADRE FORMPROMOTIONAL************************/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    nombre,
                    REPLACE(nombre, ' ', '') AS newcode
                    FROM scraper_product
                    WHERE
                    empresa LIKE '%FORPROMO%'
                ) AS b ON b.nombre = a.nombre
                SET
                a.clave_madre = b.newcode
                WHERE
                a.empresa LIKE '%FORPROMO%';

                /*****************UPDTAE CLAVE MADRE******************************/

                UPDATE scraper_product
                SET clave_madre = codigo
                WHERE clave_madre IS NULL;

                /********* UPDATE FIELD CODIGO IN FORPROMOTIONAL *****/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    codigo,
                    nombre,
                    color,
                    CONCAT(REPLACE(nombre, ' ', ''), color) AS newcode
                    FROM scraper_product
                    WHERE
                    empresa LIKE '%FORPROMO%'
                ) AS b ON b.nombre = a.nombre
                SET
                a.codigo = b.newcode
                WHERE
                a.nombre = b.nombre
                AND
                a.color = b.color
                AND
                a.empresa LIKE '%FORPROMO%';

                /**************** ADD CODIGO MORE COLOR TO CODIGO IF CLAVEMADRE = CODIGO ****************/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    empresa,
                    codigo,
                    clave_madre,
                    color,
                    CONCAT(codigo, REPLACE(color, ' ', '')) AS newcode
                    FROM scraper_product
                    WHERE
                    codigo = clave_madre
                ) AS b ON b.codigo = a.codigo
                SET a.codigo = b.newcode
                WHERE
                a.codigo = b.clave_madre
                AND
                a.color = b.color;

                /********* UPDTAE CLAVEMADRE TO DOBLEVELA ******************/

                UPDATE scraper_product AS a
                INNER JOIN
                (
                    SELECT
                    clave_madre,
                    codigo,
                    CONCAT(LEFT(codigo, 5), LEFT(REPLACE(nombre, ' ', ''), 5)) AS newcode,
                    nombre
                    FROM
                    scraper_product
                    WHERE
                    empresa LIKE '%DOBLEVELA%'
                    AND
                    codigo NOT LIKE '%.%'
                )AS b ON b.codigo = a.codigo
                SET
                a.clave_madre = b.newcode
                WHERE
                a.empresa LIKE '%DOBLE%'
                AND
                a.codigo NOT LIKE '%.%'
                AND
                a.codigo = b.codigo;

                /*UPDATE FIELD CLAVEMADRE FROM CODIGO IF NOT HAVE PREVIUS RULES OF CODIGO */

                UPDATE
                scraper_product
                SET
                categoria = REPLACE(categoria, 'http://www.forpromotional.com.mx/index.php/virtuemart/categories-layout/', '');

                UPDATE
                scraper_product
                SET
                categoria = REPLACE(categoria, 'http://www.forpromotional.com.mx/index.php/virtuemart/', '');

                UPDATE
                scraper_product
                SET
                categoria = REPLACE(categoria, 'catalogo&amp;cat=', '');

                UPDATE
                scraper_product
                SET
                codigo = CONCAT(codigo,'___NA')
                WHERE
                clave_madre = codigo;

                UPDATE
                scraper_product
                SET
                color = CONCAT(color,'NA')
                WHERE
                color = ''
                OR
                color IS NULL;

                UPDATE scraper_product SET descripcion = REPLACE(descripcion, ';', '.');

                """

    def get_unique_products(self):
        """
        get all unique products.
        """
        self._cursor = self._conn.cursor()

        query_prod = """
                    SELECT
                    Empresa,
                    Codigo
                    FROM Scraper_Datos
                    GROUP BY Empresa, Codigo
                    """
        self._cursor.execute(query_prod)
        prod = self._cursor.fetchall()
        self._cursor.close()

        return prod

    def get_top5_stockprice_products(self, code):
        """
        get the last 5 stock of run scraper.
        """
        self._cursor = self._conn.cursor()

        query_top = """
                SELECT
                TOP 5
                CONVERT(VARCHAR, c.Fecha) AS Fecha,
                CONVERT(VARCHAR, c.Existencia) AS Existencia,
                CONVERT(VARCHAR, c.Precio) AS Precio
                FROM Scraper_Datos AS c
                WHERE
                c.Codigo ='"""+code+"""'
                GROUP BY c.Fecha, c.Existencia, c.Precio
                ORDER BY c.Fecha DESC
                """
        self._cursor.execute(query_top)
        top = self._cursor.fetchall()
        self._cursor.close()

        return top
